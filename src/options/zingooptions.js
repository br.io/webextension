// zingooptions.js
// 
// this file is for scripts to run in our options page, if we have one.
//
// following our 'there can be only one' button idea, I think this might be a good spot to deal with details, if we let our users have any choices on preferences, ways to interact, etc.
