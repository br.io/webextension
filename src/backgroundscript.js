/* background-script.js

this is for long-running/persistant logic when ZingO is engaged - ie, what runs in the background independent of any particular browser window or tab. It has its own global window object.
background scripts can use most WebExtension APIs 
they also can load content scripts into webpages, however,
they do not get direct access to web pages (maybe a bonus from a security context? we could maybe handle our money-stuff, or ID stuff, etc.)
if multiple scripts are defined in the manifest, they all run in the same context.
if loaded with an HTML page indirectly, ES6 modules can also be loaded.

next lines are just to test loading, etc.


let bool = true

let str = "stringy."

let ar = ["ar string, index 0", 1, Infinity, NaN, undefined, null, {"object entry1": "stringof 1", "object entry2": 2}, function(){return "test function in ar working."} ]

let testFunc = function(){
    return "testFunc working."
}

var testVarFunc = function(){
    return "testVarFunc working."
}

let currentDate = new Date()

let testRegExp = new RegExp(/a-z/)
*/
